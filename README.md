Sync UUIDs!

During development, you often have to upload code from the
version control system to multiple servers. You can use this
module to synchronize the configuration. It can be useful if
you install to the site a module containing configurations
(in this case, the module will create configurations on the
site with different UUIDs on each server, so you need to sync
them).

How to use: 
  drush su OR drush sync-uuids

Author: Andrew Answer https://answe.ru
